<h1>Newbie info</h1>
<p>This page is intended to give basic information to those who are new to the place - either to hosting in general or Feral in particular. It's a good starting point for many of the questions you may have.</p>
<p>You might also find use for our <a href="glossary">glossary</a>, if some of the terms are unfamiliar to you.</p>

<details id="toc" open>
    <summary>Table of contents</summary>
    <nav>
        <ol>
            <li><a href="#buying-slot">Buying a slot</a></li>
            <li><a href="#using-slot">Using the slot</a></li>
            <li><a href="#common-questions">Common questions</a></li>
        </ol>
    </nav>
</details> 

<h2 id="buying-slot">Buying a slot</h2>
<p> The <a href="https://www.feralhosting.com/pricing">pricing page</a> gives you the slot specs and cost for our capacity (HDD) and capability (SSD) slots. It also gives their expected availability.</p>
<p>Set up times are quoted as instant, but this is completely dependent on funds actually being transferred to us, not the completion of the payment process. The system is automated so as soon as we actually receive the funds the payment process completes and the slot is created. You are then notified by email that your slot is ready to use.</p>
<p>There's a page on <a href="payment">payment problems</a> which might come in useful and ultimately, if you have any issues when buying a slot please <a href="tickets">open a ticket</a>.</p>

<h2 id="using-slot">Using the slot</h2>
<p>Your server is a remote device. What does this mean? It means that your Feral slot is totally separate from your physical location. Like a computer in another person's house. This is often where people get confused, regarding what is done on their home devices and what the server does.</p>
<p>When you buy a slot from Feral it will be activated and created on a server in the carrier-neutral Interxion data centre in Netherlands. No matter where in the world you are, you will be connecting to this location when you directly access your slot's features. All outgoing traffic from your Feral slot originates from this Netherlands location.</p>

<h3>How are the slots set up?</h3>
<p>The server your slot is running on Linux in a <dfn>headless</dfn> environment - this means it doesn't have a graphical display with which to administer it but can be managed remotely. Each server has several user disks and on each disk the users get their own directory, corresponding to their username. A new user directory comes with default subdirectories and looks like this:</p>
<pre><samp>.
&#x251c;&#x2500;&#x2500; action.log
&#x251c;&#x2500;&#x2500; private
&#x2514;&#x2500;&#x2500; www
    &#x2514;&#x2500;&#x2500; <var>user</var>.<var>server</var>.feralhosting.com
        &#x2514;&#x2500;&#x2500; public_html

4 directories, 1 file</samp></pre>
<p>The directory tree above contains the following:</p>
<dl>
    <dt>action.log</dt>
    <dd>A file logging automated system restarts of software running on your slot</dd>
    <dt>private</dt>
    <dd>Software you can install via the Feral web manager will live here. "Private" does not mean the other directories are public - it's named private for historic reasons</dd>
    <dt>www</dt>
    <dd>The root directory for your domains and publicly served data</dd>
    <dt><var>user</var>.<var>server</var>.feralhosting.com</dt>
    <dd>The domain each Feral user gets by default - it's possible to <a href="/wiki/slots/custom-domain">add further custom domains</a></dd>
    <dt>public_html</dt>
    <dd>The contents of this directory will be publicly served but can be password-protected</dd>
</dl>

<p>Given that the slot is always, running elsewhere but accessible remotely - how do we actually do things with it? There are lots of ways! This guide briefly discusses many of them and links through to further information.</p>

<h3>Via a web browser</h3>
<p>A common way to access the software installed on your slot is through your web browser. The torrent clients <a href="../software/rutorrent">ruTorrent</a>, <a href="../software/deluge">Deluge</a> and <a href="../software/transmission">Transmission</a> are each accessible via a web UI, as are many other of the programs featured in the guides of this wiki's <a href="/wiki#software">software section</a>. Please see the pages in that section for usage help and tips.</p>
<p>Another way to use your slot via your browser is to host something on it. Every user gets their own domain (in the form <samp>https://server.feralhosting.com/user/</samp>) but it's also possible to host your own custom domain. Whether using the default or a custom domain, the basic idea is that anything in that domain's <samp>public_html</samp> directory (see the directory tree example above) will be publicly served up.</p>

<h3>Via FTP/SFTP</h3>
<p>Transferring from your slot to your home and managing the data on your slot can be done in several ways, but perhaps the most common is via FTP/SFTP. There is a separate guide to <a href="../slots/ftp">getting connected and using SFTP clients</a>.</p>

<h3>Via SSH</h3>
<p>SSH (Secure Shell) is a good way to manage your slot's contents to a greater extent than FTP or web UIs allow - some software can only be installed by running SSH commands, for example. There is a separate guide to <a href="../slots/ssh">connecting to your slot via SSH.</a> The <a href="/wiki/">wiki page</a> has a list of and guides to some of the software which can be installed.</p>

<h3>OpenVPN</h3>
<p>Each slot comes with the ability to use your slot as a VPN. This means every program on your computer will go through the Feral slot and the server will be the point of origin for your traffic to the rest of the world. There is a separate guide to <a href="../software/openvpn">installing and configuring OpenVPN</a> at Feral.</p>
<p>Each OpenVPN installation is limited to a single open connection at any one time. You cannot have multiple OpenVPN clients establish an open connection to your Feral OpenVPN server at the same time. So this means only one device behind a firewall/router would be able to connect directly to and through the Feral slot OpenVPN server. Some routers will allow you to configure and connect to the OpenVPN server from directly within the router set-up allowing all users behind to connect via the Feral Slot OpenVPN server, but Feral are unable to provide specific help for different router models. To have multiple connections you can instead use <a href="../slots/ssh-tunnels">SSH tunnels</a>.</p>

<h2 id="common-questions">Common questions</h2>
<p>This section contains some of the more common questions you might have. If your question still isn't answered, please <a href="tickets">open a ticket</a>.</p>

<h3>Payments</h3>

<h4>How can I renew the slot?</h4>
<p>The link to pay can be found on the slot's Summary page - please look for the link entitled "Renew slot".</p>

<h4>How can I pay? Do you support PayPal?</h4>
<p>We cannot offer PayPal as a payment option - their terms are too onerous and allow them (for example) to hold on to customer's money with no warning. We do, however, accept all major debit / credit cards via Stripe and anonymous payments via Bitcoin.</p>

<h3>Slot info</h3>

<h4 id="server-ip">How do I find my server's IP?</h4>
<p>The easiest way to do this is simply to ping the fully qualified domain name (FQDN) from any terminal or command prompt. The FQDN is <var>server</var>.feralhosting.com, where <var>server</var> is replaced by the actual name of the server. So, to give an example from Windows PowerShell:</p>
<pre><samp>PS C:\Users\Example> ping zeus.feralhosting.com

Pinging zeus.feralhosting.com [185.21.216.140] with 32 bytes of data:
Reply from 185.21.216.140: bytes=32 time=24ms TTL=51
Reply from 185.21.216.140: bytes=32 time=24ms TTL=51
Reply from 185.21.216.140: bytes=32 time=24ms TTL=51
Reply from 185.21.216.140: bytes=32 time=24ms TTL=51

Ping statistics for 185.21.216.140:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 24ms, Maximum = 24ms, Average = 24ms</samp></pre>
<p>From the output, we can see that the IP of the server Zeus is <samp>185.21.216.140</samp>.</p>
<p>The scope of this guide does not extend to opening a terminal / command prompt for each operating system - please consult the documentation for yours.</p>

<h4>How do I get more disk space?</h4>
<p>There is no way to provide a slot with increased storage capacity once it has been set up. In order to increase storage capacity or to take advantage of offers related to storage capacity you will be required to purchase a new slot. You can start the upgrade process yourself at any time - click on the <samp>Summary</samp> link to the left-hand side, then look for <samp>Upgrade / change slot</samp> under the list of actions.</p>

<h4>You're selling slots for the same price but more disk space?</h4>
<p>We are constantly striving to improve our products and offer the best available value to the user. This means that slots with improved storage capacity may be available in the future. Unfortunately there is no way to provide a slot with increased storage capacity once it has been set up, so this is not something can apply to your slot automatically.</p>

<h4>Does my slot have a dedicated IP?</h4>
<p>Your server has a dedicated IP that is shared by all the users on your server. So while your slot does have a dedicated IP, this IP is not unique to you.</p>

<h4>Is my slot a dedicated server? Is this a VPS?</h4>
<p>No, Feral does not provide any dedicated servers or a VPS type service. Your slot will be on a powerful server that is shared by other users. Your account will be a user account on the currently running Linux OS that powers the servers.</p>

<h4>Where is my slot located? Where are the the servers hosted?</h4>
<p>The servers are hosted at the <a href="http://www.interxion.com/">Interxion</a> carrier-neutral data centre in the Netherlands.</p>

<h4>What version of Linux is running on my slot?</h4>
<p>Your slot is running on Debian Stretch. This SSH command will show you your current Debian version:</p>
<p><kbd>lsb_release -d</kbd></p>

<h3>Using the slot</h3>

<h4>How can I improve speeds?</h4>
<p>Please see our separate guide to <a href="../slots/more-speed">improving speeds</a>.</p>

<h4>Can I send emails from my slot?</h4>
<p>Email has been blocked due to abuse from people attempting to spam. If you need to send email from your custom software you may be able to configure them to use an external smtp service.</p>

<h4>How can I check my disk usage?</h4>
<p>The summary page will update the disk space used every day but can be checked manually by clicking the <samp>Check Now</samp> button. Look for the <samp>Summary</samp> link to the left-hand side. You can also please log in via SSH and execute the following:</p>
<p><kbd>du -sB GB ~/</kbd></p>
<p>It might also be possible to find out the space usage using your FTP software. The method may change depending on the FTP software so please consult their documentation on this.</p>

<h4>Can I install my own software? Will staff install it for me?</h4>
<p>Users can install software to their home directory but won't have the privileges to run <samp>sudo</samp>, <samp>su</samp> and <samp>apt-get</samp>. Please read the <a href="../slots/generic-install-guide">generic guide to installation</a> for more information.</p>
<p>Staff will not generally install unsupported software for you, but if you encounter any difficulties please feel free to <a href="https://www.feralhosting.com/tickets/new">open a ticket</a>. If you require packages and dependencies that are part of the Debian Stretch, open a ticket and ask staff if they can install them.</p>

<h4 id="pathfix">When I try to use software I've installed, I get &lt;Command&gt; not found</h4>
<p>This happens because the location (usually a <samp>bin</samp> directory) is not part of the PATH variable. This PATH is a way of telling the system where it should look to run the software. While the system will automatically add your <samp>bin</samp> directory on logging in, you may be installing something to <samp>bin</samp> for the first time then trying to use the software straight away.</p>

If <samp>bin</samp> is not part of PATH, run the command <kbd>echo $PATH</kbd> and you should see the following:
<p><samp>/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games</samp></p>
<p>If your own <samp>bin</samp> directory actually exists, you can either log out then back in to add it, or run the following command:</p>
<p><kbd>source ~/.profile</kbd></p>
<p>You'll see this change:</p>
<pre><samp>[ophion ~] echo $PATH
/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games
[ophion ~] source ~/.profile
[ophion ~] echo $PATH
/media/<var>diskname</var>/<var>user</var>/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games
</samp></pre>
<p>As you might imagine, <var>diskname</var> and <var>user</var> will be replaced with your details.</p>
<p>In short - to fix, either:</p>
<ul>
    <li>Log out of SSH and log back in; or</li>
    <li>Run the command <kbd>source ~/.profile</kbd></li>
</ul>

<h4>Can I use public trackers with my slot?</h4>
<p>Feral supports and allows the use of public trackers. No need to ask or get permission. You will likely find <a href="../software/deluge">Deluge</a> has better support for them than ruTorrent.</p>

<h4>Can I run a game server on my slot?</h4>
<p>We do not allow game servers of any kind to be run on our slots.  Most happen to be java-based and are known for eating up server resources.</p>

<h4>Can I run a Tor node on my slot?</h4>
<p>We do not allow Tor exit nodes to be run on our servers. They're open invitations for trouble, and while Tor serves a useful purpose our network is not the place for it. Tor relays are fine provided they strictly only act as an intermediary. We may make an exception to this rule if you bring your own RIPE IPs and handle abuse directly while taking full responsibility.</p>

<h4>Can I share my account?</h4>
<p>Feral only supports one user account. This means they will only provide and support one set of user credentials (your account username and password). If you wish to share these credentials you can, but you do so at your own risk. Anyone you share you SSH/SFTP/FTP pass with will have as much control/access to and over the slot as you do.</p>
<p>It is possible to install a version of the FTP daemon ProFTPd to your slot in order to add users with restricted access, but staff cannot provide much more beyond general help for this, and no ongoing support.</p>