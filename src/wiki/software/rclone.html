<h1>rclone</h1>
<aside class="alert note">For permissions reasons, Feral are unable to install the FUSE module to the kernel.</aside>
<p>This article will show you how to install rclone and setup remotes for it. Rclone can be used to sync files and directories to and from various cloud storage providers (and your local filesystem).</p>
<p>You'll need to execute some commands via SSH to use this software. There is a separate guide on how to <a href="../slots/ssh">connect to your slot via SSH</a>. Commands are kept as simple as possible and in most cases will simply need to be copied and pasted into the terminal window (then executed by pressing the <kbd>Enter</kbd> key).</p>

<details id="toc" open>
    <summary>Table of contents</summary>
    <nav>
        <ol>
            <li><a href="#installation">Installation</a></li>
            <li><a href="#configure">Configuring</a>
                <ul>
                    <li><a href="#onedrive">Microsoft OneDrive</a></li>
                    <li><a href="#googleo">Google Drive</a></li>
                    <li><a href="#encryption">Creating a remote for encryption/decryption</a></li>
                </ul>
            </li>
            <li><a href="#usage">Using rclone</a>
                <ul>
                    <li><a href="#example-commands">Example commands</a></li>
                </ul>
            </li>
            <li><a href="#uninstallation">Uninstallation</a></li>
            <li><a href="#external-links">External links</a></li>
        </ol>
    </nav>
</details> 

<h2 id="installation">Installation</h2>
<p>Install rclone by logging in via SSH, then copying and pasting the following:</p>
<pre><kbd>mkdir -p ~/bin &amp;&amp; bash
wget -qO ~/rclone.zip https://downloads.rclone.org/rclone-current-linux-amd64.zip
unzip ~/rclone.zip -d ~/
mv ~/rclone-v*-linux-amd64/rclone ~/bin
rm -rf ~/rclone-v*-linux-amd64 ~/rclone.zip</kbd></pre>
<p>If your binary directory (<samp>~/bin</samp>) is not in PATH you'll need to give the full path when trying to run the software in that directory. A better alternative is to <a href="/wiki/getting-started/newbie-info#pathfix">add the bin directory to PATH</a>.</p>
<p>The rest of the guide assumes you have done this and will simply use <kbd>rclone</kbd> to run the rclone binary, instead of <kbd>~/bin/rclone</kbd>.</p>

<h2 id="configure">Configuring</h2>
<p>Although rclone can send to many different services, only Microsoft OneDrive and Google Drive are currently covered. Feel free to add others, bearing in mind the format and style below.</p>

<h3 id="onedrive">Microsoft OneDrive</h3>
<p>Since Feral's servers are headless (without a graphical environment), you'll need to configure OneDrive in two parts - the first on your slot and the second on your own computer.</p>
<p>On your slot here at Feral, run the rclone's configuration wizard with <kbd>rclone config</kbd>. You'll be presented with a lot of steps to go through:</p>
<ol>
    <li>Add a new remote with <kbd>n</kbd></li>
    <li>Name it as you wish (for example, <samp>onedrive</samp>)</li>
    <li>Select OneDrive by entering <kbd>onedrive</kbd></li>
    <li>Press <kbd>enter</kbd> to skip through the <samp>client_id</samp> option</li>
    <li>Press <kbd>enter</kbd> to skip through the <samp>client_secret</samp> option</li>
    <li>As it is a headless machine, you'll need to type <kbd>n</kbd> in response to <samp>Use auto config?</samp></li>
</ol>

<p>Once you've done this you'll see the following:</p>
<pre><samp>For this to work, you will need rclone available on a machine that has a web browser available.
Execute the following on your machine:
        rclone authorize "onedrive"
Then paste the result below:</samp></pre>

<p>At this point you'll need to download rclone to your own computer and run a command on it. If on Windows, run <kbd>rclone.exe authorize "onedrive"</kbd>; on Linux you can run <kbd>rclone authorize "onedrive"</kbd>.</p>
<p>Your browser should open, giving you an opportunity to log in to your OneDrive account. After logging in successfully you'll be given a code - copy and paste this into your SSH terminal window.</p>
<p>Once done, you confirm the information with <kbd>y</kbd> then press <kbd>q</kbd> to quit the config.</p>

<h3 id="google">Google Drive</h3>
<p>Execute <kbd>rclone config</kbd> to bring up the config menu, then go through the steps as below.</p>
<ol>
    <li>Add a new remote with <kbd>n</kbd></li>
    <li>Name it as you wish (for example, <samp>gdrive</samp>)</li>
    <li>Select Google Drive by entering <kbd>drive</kbd></li>
    <li>Press <kbd>enter</kbd> to skip through the <samp>client_id</samp> option</li>
    <li>Press <kbd>enter</kbd> to skip through the <samp>client_secret</samp> option</li>
    <li>Press <kbd>enter</kbd> to skip through the <samp>service_account_file</samp> option</li>
    <li>As it is a headless machine, you'll need to type <kbd>n</kbd> in response to <samp>Use auto config?</samp></li>
</ol>
<p>Once you've done this you'll see the following (where <var>$URL</var> is replaced by an actual URL):</p>
<pre><samp>If your browser doesn't open automatically go to the following link: <var>$URL</var>
Log in and authorize rclone for access
Enter verification code></samp></pre>
<p>Copy that URL into your browser and login in to grant authorisation. Then, copy and paste the code you get back into the terminal window.</p>
<p>Once done, you confirm the information with <kbd>y</kbd> then press <kbd>q</kbd> to quit the config.</p>

<h3 id="encryption">Creating a remote for encryption/decryption</h3>
<p>Creating this type of remote will allow you to encrypt the files you transfer to your "regular" remote and decrypt them on the way back.</p>

<ol>
    <li>Add a new remote with <kbd>n</kbd></li>
    <li>Name it as you wish (for example, <samp>crypt</samp>)</li>
    <li>Select Encrypt/Decrypt a remote by entering <kbd>crypt</kbd></li>
    <li>Provide the name of the remote and the path on it you wish to encrypt.</li>
    <li>Configure your passwords - generating might mean maximum security but you should make a note of the passwords somewhere</li>
</ol>
<p>Once done, you confirm the information with <kbd>y</kbd> then press <kbd>q</kbd> to quit the config.</p>

<h2 id="usage">Using rclone</h2>
<p>This section will give some brief information on using rclone. As at the top of this page it will not be possible for staff to install <samp>FUSE</samp> for you, so you will not be able to encrypt the transfers with rclone.</p>

<p>Here are some commands you can run with rclone. To run them, simply execute <kbd>rclone <var>command</var></kbd> where <var>command</var> is replaced by the actual command. Examples can be found after in the section after.</p>
<dl>
    <dt>listremotes</dt>
    <dd>This lists the remotes you've set up.</dd>
    <dt>sync <var>feral_path</var> <var>remote</var>:<var>remote_path</var></dt>
    <dd>This syncs the data on your slot to the remote. It adds files which don't exist on the remote (or have been changed), skips over data which is already present and deletes files on the remote which have also been deleted on your Feral slot.</dd>
</dl>
<p>Please note that variables are used in the example commands above. These variables should be replaced as follows:</p>
<dl>
    <dt><dfn>feral_path</dfn></dt>
    <dd>The path to the file/folder which is on your slot here at Feral</dd>
    <dt><dfn>remote</dfn></dt>
    <dd>The name of the remote</dd>
    <dt><dfn>remote_path</dfn></dt>
    <dd>The path to the file/folder on your remote</dd>
</dl>

<h3 id="example-commands">Example commands</h3>
<p><samp>rclone sync ~/private/folder gdrive:/feral</samp></p>
<p>The above command would sync a directory named <samp>folder</samp> in the private directory of your Feral slot to a directory named <samp>feral</samp> on your remote, named <samp>gdrive</samp></p>

<h2>Uninstallation</h2>
<aside class="alert note">The commands below will completely remove the software and its config files - back up important data first!</aside>
<pre><kbd>kill -9 "$(pgrep -fu "$(whoami)" "rclone")"
rm -rf ~/bin/rclone ~/.config/rclone</kbd></pre>

<h2>External links</h2>
<ul>
    <li><a href="https://rclone.org/">Rclone's homepage</a></li>
</ul>