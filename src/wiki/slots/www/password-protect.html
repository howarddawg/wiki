<h1>Password protecting your WWW directory</h1>
<p>This guide will show you how to password protect things in your <samp>public_html</samp> directory. By default anything in it will be publicly-visible and accessible. You can prevent this by password protecting the directories. This will also stop web crawlers from being able to index your data.</p>
<p>You'll need to execute some commands via SSH to use this software. There is a separate guide on how to <a href="../ssh">connect to your slot via SSH</a>. Commands are kept as simple as possible and in most cases will simply need to be copied and pasted into the terminal window (then executed by pressing the <kbd>Enter</kbd> key).</p>

<details id="toc" open>
    <summary>Table of contents</summary>
    <nav>
        <ol>
            <li><a href="#nginx">Nginx</a>
                <ul>
                    <li><a href="#nginx-config-create">Create the .conf file</a></li>
                </ul>
            </li>
            <li><a href="#apache">Apache</a>
                <ul>
                    <li><a href="#apache-config-create">Create the .htaccess file</a></li>
                    <li><a href="#apache-quick">Quick - using the ruTorrent details to protect your links directory</a></li>
                </ul>
            </li>
            <li><a href="#htpasswd-create">Create the .htpasswd file</a></li>
            <li><a href="#troubleshooting">Troubleshooting</a></li>
        </ol>
    </nav>
</details> 

<p>The guide changes depending on whether you use apache or <a href="...software/nginx">nginx</a> - please scroll to the relevant section below. The default is currently nginx but you can check which you're running by logging in via SSH and running the following command:</p>
<p><kbd>ps x</kbd></p>
<p>You'll get a list of processes. Look for one of the two different processes listed below to find out whether you're running Apache or Nginx.</p>
<dl>
    <dt>/usr/sbin/apache2 -k start</dt>
    <dd>You're running Apache (there may be several of these processes listed - that's fine)</dd>
    <dt>nginx: master process /usr/sbin/nginx -c <var>$home</var>/.nginx/nginx.conf</dt>
    <dd>You're running nginx (<var>$home</var> above will be replaced by the path to your home directory)</dd>
</dl>

<h2 id="nginx">nginx</h2>
<p>Nginx does not use <samp>.htaccess</samp> files. Instead, everything is contained in config files which specify the location and options. All locations are relative to the WWW root (in our case, the public_html directory).</p>
<p>This section assumes you wish to protect a directory named <samp>links</samp> - please alter your commands if you wish to protect another location.</p>

<h3 id="nginx-config-create">Create the .conf file</h3>
<p>First, we need some information that will go into our <samp>.conf</samp> file, namely the future location of our <samp>.htpasswd</samp> file. To get this information, simply copy and paste the following:</p>
<p><kbd>echo $HOME/private/.htpasswd</kbd></p>
<p>Copy the result to an open notepad document or similar, as we'll need it in a minute.</p>

<p>You can then start to create the <samp>.conf</samp> file. This guide will use Nano and as above takes a links directory as its example. To start writing the file, copy and paste:</p>
<p><kbd>nano ~/.nginx/conf.d/000-default-server.d/links.conf</kbd></p>
<p>Then, copy and paste the following into the document, changing <var>passwd_path</var> to the <samp>.htpasswd</samp> file location you generated earlier:</p>
<pre><code>location /links {
    auth_basic "Please log in";
    auth_basic_user_file <var>passwd_path</var>;
}</code></pre>

<p>Once you're done hold <kbd>ctrl</kbd> + <kbd>x</kbd> to save. Press <kbd>y</kbd> to confirm.</p>
<p>Finally, you need to reload the nginx configs by copying and pasting:</p>
<p><kbd>/usr/sbin/nginx -s reload -c ~/.nginx/nginx.conf</kbd></p>
<p>You can now move down to the section below, 'Create the .htpasswd file'.</p>

<h2 id="apache">Apache</h2>
<p>Apache uses <samp>.htaccess</samp> files - these provide special instructions that Apache will interpret and apply to the web-server.</p>
<p>To password protect a directory you must use the <samp>.htaccess</samp> together with a <samp>.htpasswd</samp> file that stores the encrypted username and password information for created users.</p>

<h3 id="apache-config-create">Create the .htaccess file</h3>
<p>You can use a text editor via SSH to create the <samp>.htaccess</samp> file at the location to be protected. This section assumes you wish to protect a directory named <samp>links</samp> - please alter your commands if you wish to protect another location.</p>
<p>Please note that the <samp>.htaccess</samp> file will restrict the directory its placed in and its children.</p>

<p>First, we need some information that will go into our <samp>.htaccess</samp> file, namely the future location of our <samp>.htpasswd</samp> file. To get this information, simply copy and paste the following:</p>
<p><kbd>echo $HOME/private/.htpasswd</kbd></p>
<p>Copy the result to an open notepad document or similar, as we'll need it in a minute.</p>

<p>You can then start to create the <samp>.htaccess</samp> file. This guide will use Nano and as above takes a links directory as its example. To start writing the file, copy and paste:</p>
<p><kbd>nano ~/www/$(whoami).$(hostname -f)/public_html/links/.htaccess</kbd></p>
<p>Then, copy and paste the following into the document, changing <var>passwd_path</var> to the <samp>.htpasswd</samp> file location you generated earlier:</p>
<pre><code>AuthType Basic
AuthName "Secure Area"
AuthUserFile "<var>passwd_path</var>"
Require valid-user</code></pre>

<p>Once you're done hold <kbd>ctrl</kbd> + <kbd>x</kbd> to save. Press <kbd>y</kbd> to confirm.</p>
<p>Finally, you need to change the permissions by copying and pasting:</p>
<p><kbd>chmod 600 ~/www/$(whoami).$(hostname -f)/public_html/links/.htaccess</kbd></p>
<p>You can now move down to the section below, 'Create the .htpasswd file'.</p>

<h3 id="apache-quick">Quick - using the ruTorrent details to protect your links directory</h3>
<p>If you have ruTorrent installed and all you want to do is password protect your <a href ="dl-via-http">links</a> directory, simply copy and paste the following and press <kbd>enter</kbd>:</p>
<p><kbd>echo -e "AuthType Basic\nAuthName \"$(whoami)\"\nAuthUserFile \"$HOME/www/$(whoami).$(hostname -f)/public_html/rutorrent/.htpasswd\"\nRequire valid-user" > ~/www/$(whoami).$(hostname -f)/public_html/links/.htaccess</kbd></p>

<h2 id="htpasswd-create">Create the .htpasswd file</h2>
<p>Both the apache <samp>.htacess</samp> file and nginx's config files point to <samp>~/private/.htpasswd</samp> for the authentication details, so this process is the same for both Apache and Nginx.</p> 
<p>Create the <samp>.htpasswd</samp> file by copying and pasting the following (replacing <var>username</var> with a username you want):</p>

<p><kbd>htpasswd -cm ~/private/.htpasswd <var>username</var></kbd></p>
<p>You'll be asked to type in and confirm your password. Please note that the password entry will <em>not</em> appear to respond to your input - it will not display <samp>*****</samp> or something similar as you type.</p>
<p>Finally, you need to change the permissions by copying and pasting:</p>
<p><kbd>chmod 600 ~/private/.htpasswd</kbd></p>

<aside class="alert note">Once created, use htpasswd -m ~/private/.htpasswd <var>username</var> if you want to add further users: the <samp>c</samp> option will overwrite previous entries.</aside>

<h2 id="troubleshooting">Troubleshooting</h2>
<p><span>I get an <samp>Internal server error</samp> message when visiting my page</span></p>
<p>This is likely because something was mistyped in your <samp>.htaccess</samp> file - please double-check it for errors, making sure the locations are valid and correct.</p>